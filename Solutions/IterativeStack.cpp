#include <Solution.hpp>
using namespace tsp;

#include <limits>

Tour solve(const Digraph& digraph)
{
    Tour best{digraph}, current{digraph};
    best.cost = std::numeric_limits<int>::max();

    Visited visited;
    visited.resize(digraph.nodes.size(), false);
    visited[0] = true;

    std::vector<Edge> stack;
    stack.assign(digraph.nodes[0].edges.rbegin(), digraph.nodes[0].edges.rend());

    while (!stack.empty())
    {
        auto top = stack.back();
        stack.pop_back();

        if (top.start == SIZE_MAX)
        {
            visited[top.end] = false;
            current.pop();
            continue;
        }

        visited[top.end] = true;
        current.push(top);

        if (current.edges.size() + 1 == digraph.nodes.size())
        {
            current.push(digraph.nodes[current.top().end].find_edge(0));
            if (current.cost < best.cost)
            {
                best.copy(current);
            }
            current.pop();
            visited[top.end] = false;
            current.pop();
            continue;
        }

        stack.emplace_back(SIZE_MAX, top.end, -1);
        for (auto edge : reverse(digraph.nodes[top.end].edges))
        {
            if (!visited[edge.end] && current.cost + edge.cost < best.cost)
            {
                stack.push_back(edge);
            }
        }
    }

    return best;
}

TSP_SOLUTION(solve)
