#include <Solution.hpp>
using namespace tsp;

struct PartialTour
{
    Tour tour;
    Visited visited;

    PartialTour(const Digraph& digraph, Edge edge) : tour{digraph}
    {
        tour.push(edge);
        visited.assign(digraph.nodes.size(), false);
        visited[0] = true;
        visited[edge.end] = true;
    }
};

Tour solve(const Digraph& digraph)
{
    Tour best{digraph};
    best.cost = std::numeric_limits<int>::max();

    std::vector<PartialTour> stack;
    for (auto edge : reverse(digraph.nodes[0].edges))
    {
        stack.emplace_back(digraph, edge);
    }

    while (!stack.empty())
    {
        auto tour = std::move(stack.back());
        stack.pop_back();

        if (tour.tour.edges.size() + 1 == digraph.nodes.size())
        {
            tour.tour.push(digraph.nodes[tour.tour.top().end].find_edge(0));
            if (tour.tour.cost < best.cost)
            {
                best.copy(tour.tour);
            }
            continue;
        }

        for (auto edge : reverse(digraph.nodes[tour.tour.top().end].edges))
        {
            if (!tour.visited[edge.end] && tour.tour.cost + edge.cost < best.cost)
            {
                auto copy = tour;
                copy.tour.push(edge);
                copy.visited[edge.end] = true;
                stack.push_back(std::move(copy));
            }
        }
    }

    return best;
}

TSP_SOLUTION(solve)
