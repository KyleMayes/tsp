#include <Solution.hpp>
using namespace tsp;

#include <limits>

void solve_inner(const Digraph& digraph, Tour& best, Tour& current, Visited& visited, size_t node)
{
    if (current.edges.size() + 1 == digraph.nodes.size())
    {
        current.push(digraph.nodes[current.top().end].find_edge(0));
        if (current.cost < best.cost)
        {
            best.copy(current);
        }
        current.pop();
        return;
    }

    for (auto edge : digraph.nodes[node].edges)
    {
        if (!visited[edge.end] && current.cost + edge.cost < best.cost)
        {
            visited[edge.end] = true;
            current.push(edge);
            solve_inner(digraph, best, current, visited, edge.end);
            current.pop();
            visited[edge.end] = false;
        }
    }
}

Tour solve(const Digraph& digraph)
{
    Tour best{digraph}, current{digraph};
    best.cost = std::numeric_limits<int>::max();

    Visited visited;
    visited.resize(digraph.nodes.size(), false);
    visited[0] = true;

    solve_inner(digraph, best, current, visited, 0);

    return best;
}

TSP_SOLUTION(solve)
