#include <Digraph.hpp>

#include <algorithm>
#include <exception>
#include <iostream>
#include <stdexcept>
#include <utility>

namespace tsp
{

Edge::Edge(size_t start, size_t end, int cost) : start{start}, end{end}, cost{cost} { }

bool operator==(Edge left, Edge right)
{
    return left.start == right.start && left.end == right.end && left.cost == right.cost;
}

bool operator!=(Edge left, Edge right)
{
    return !operator==(left, right);
}

Node::Node(size_t index, std::string name) : index{index}, name{std::move(name)} { }

Edge Node::find_edge(size_t node) const
{
    if (node < index)
    {
        return edges[node];
    }
    return edges[node - 1];
}

Digraph::Digraph(size_t size, const std::vector<int>& costs)
{
    // Generate nodes.
    nodes.reserve(size);
    for (size_t i = 0; i < size; ++i)
    {
        nodes.emplace_back(i, std::to_string(i));
    }

    // Generate edges.
    for (size_t i = 0; i < size; ++i)
    {
        auto start = &nodes[i];
        start->edges.reserve(size - 1);
        for (size_t j = 0; j < size; ++j)
        {
            if (i != j)
            {
                start->edges.emplace_back(i, j, costs[(i * size) + j]);
            }
        }
    }
}

}
