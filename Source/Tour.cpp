#include <Tour.hpp>

#include <algorithm>

namespace tsp
{

Tour::Tour(const Digraph& digraph) : digraph{std::addressof(digraph)}, cost{0}
{
    edges.reserve(digraph.nodes.size() - 1);
}

Tour::Tour(const Digraph& digraph, const std::vector<size_t>& indices) : Tour(digraph)
{
    edges.reserve(digraph.nodes.size());
    for (size_t i = 1; i < indices.size(); ++i)
    {
        push(digraph.nodes[indices[i - 1]].find_edge(indices[i]));
    }
}

Edge Tour::top() const
{
    return edges.back();
}

void Tour::push(Edge edge)
{
    cost += edge.cost;
    edges.push_back(edge);
}

void Tour::pop()
{
    cost -= edges.back().cost;
    edges.pop_back();
}

void Tour::clear()
{
    cost = 0;
    edges.clear();
}

void Tour::copy(const Tour& other)
{
    cost = other.cost;
    edges.assign(other.edges.begin(), other.edges.end());
}

bool operator==(const Tour& left, const Tour& right)
{
    return left.edges == right.edges;
}

bool operator!=(const Tour& left, const Tour& right)
{
    return !operator==(left, right);
}

std::ostream& operator<<(std::ostream& stream, const Tour& tour)
{
    if (!tour.edges.empty())
    {
        stream << tour.digraph->nodes[tour.edges.front().start].name;
    }
    for (auto edge : tour.edges)
    {
        stream << " -> " << tour.digraph->nodes[edge.end].name;
    }
    stream << " (cost " << tour.cost << ")";
    return stream;
}

}
