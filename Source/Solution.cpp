#include <Solution.hpp>

#include <cstring>

namespace tsp
{

template <typename T>
T parse(char* argument, const std::vector<std::pair<const char*, T>>& possibilities)
{
    for (const auto& possibility : possibilities)
    {
        if (std::strcmp(argument, possibility.first) == 0)
        {
            return possibility.second;
        }
    }
    throw std::runtime_error{"invalid argument"};
}

static const Dataset SMALL{SMALL_DIGRAPH, SMALL_TOUR};
static const Dataset LARGE{LARGE_DIGRAPH, LARGE_TOUR};

std::pair<Operation, Dataset> parse(int argc, char* argv[])
{
    if (argc != 3)
    {
        throw std::runtime_error{"invalid number of arguments (expected 2)"};
    }
    auto operation = parse<Operation>(argv[1], {{"test", Test}, {"benchmark", Benchmark}});
    auto dataset = parse<Dataset>(argv[2], {{"small", SMALL}, {"large", LARGE}});
    return {operation, dataset};
}

}
