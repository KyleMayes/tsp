#!/usr/bin/env python

import subprocess
import sys

def test(executable, dataset):
    print '  {}'.format(dataset)
    try:
        output = subprocess.check_output([executable, 'test', dataset])
        print '    Success!'
    except subprocess.CalledProcessError as e:
        print '\n'.join(['    {}'.format(l) for l in e.output.split('\n')])

for solution in sys.argv[1:]:
    print '{}:'.format(solution)
    executable = './{}'.format(solution)
    test(executable, 'small')
    test(executable, 'large')
