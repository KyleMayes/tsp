#pragma once

#include <Tour.hpp>
#include <Utility.hpp>

#include <chrono>
#include <iostream>

namespace tsp
{

/// Indicates how a solution should be used.
enum Operation
{
    /// Benchmark the performance of the solution.
    Benchmark,
    /// Test the correctness of the solution.
    Test,
};

/// A directed graph and the best tour for that graph.
using Dataset = std::pair<Digraph, Tour>;

/// Parse the command-line arguments.
std::pair<Operation, Dataset> parse(int argc, char* argv[]);

template <typename F>
void test(const Dataset& dataset, F f)
{
    auto solution = f(dataset.first);
    if (solution != dataset.second)
    {
        std::cout << "Found:    " << solution << std::endl;
        std::cout << "Expected: " << dataset.second << std::endl;
        std::exit(1);
    }
}

template <typename F>
double time(size_t samples, const Dataset& dataset, F f)
{
    using Clock = std::chrono::high_resolution_clock;
    auto start = Clock::now();
    for (size_t i = 0; i < samples; ++i)
    {
        f(dataset.first);
    }
    auto count = std::chrono::duration_cast<std::chrono::nanoseconds>(Clock::now() - start).count();
    return count / static_cast<double>(samples);
}

template <typename F>
void benchmark(const Dataset& dataset, F f)
{
    auto samples = (dataset.first.nodes.size() == 4) ? 1000000 : 4;
    std::cout << samples << ":" << time(samples, dataset, f) << std::endl;
}

/// Benchmark or test the supplied function.
template <typename F>
int run(int argc, char* argv[], F f)
{
    auto arguments = parse(argc, argv);
    switch (arguments.first)
    {
    case Test:
        test(arguments.second, f);
        break;
    case Benchmark:
        benchmark(arguments.second, f);
        break;
    default:
        break;
    }
    return 0;
}

#define TSP_SOLUTION(F) int main(int argc, char* argv[]) { return ::tsp::run(argc, argv, F); }

}
