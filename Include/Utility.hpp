#pragma once

#include <iterator>
#include <memory>

namespace tsp
{

/// A container wrapper that reverses the iterators of the wrapped container.
template <typename T>
struct Reverse
{
    T* container;

    Reverse(T& container) : container{std::addressof(container)}
    {
    }

    auto begin()
    {
        return std::make_reverse_iterator(std::end(*container));
    }

    auto end()
    {
        return std::make_reverse_iterator(std::begin(*container));
    }
};

template <typename T>
Reverse<T> reverse(T& container)
{
    return {container};
}

}
