#pragma once

#include <Digraph.hpp>

#include <ostream>

namespace tsp
{

/// Indicates which nodes in a directed graph have been visited.
using Visited = std::vector<char>;

/// An ordered visitation of all the nodes in a directed graph.
struct Tour
{
    /// The directed graph this tour traverses.
    const Digraph* digraph;
    /// The edges traversed by this tour.
    std::vector<Edge> edges;
    /// The total cost of this tour.
    int cost;

    Tour(const Digraph& digraph);
    Tour(const Digraph& digraph, const std::vector<size_t>& indices);

    /// Returns the last edge in this tour.
    Edge top() const;

    /// Adds the supplied edge to this tour.
    void push(Edge edge);

    /// Removes the last edge in this tour.
    void pop();

    /// Removes all the edges in this tour.
    void clear();

    /// Copies the supplied tour into this tour.
    void copy(const Tour& other);
};

bool operator==(const Tour& left, const Tour& right);
bool operator!=(const Tour& left, const Tour& right);

std::ostream& operator<<(std::ostream& stream, const Tour& tour);

/// The best four-city tour.
static const Tour SMALL_TOUR{SMALL_DIGRAPH, {0, 3, 1, 2, 0}};

/// The best fifteen-city tour.
static const Tour LARGE_TOUR{LARGE_DIGRAPH, {0, 10, 3, 5, 7, 9, 13, 11, 2, 6, 4, 8, 14, 1, 12, 0}};

}
