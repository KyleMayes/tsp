#pragma once

#include <string>
#include <vector>

namespace tsp
{

struct Node;

/// A directed edge between two nodes in a graph.
struct Edge
{
    /// The index of the node from which this edge originates.
    size_t start;
    /// The index of the node in which this edge terminates.
    size_t end;
    /// The cost associated with traversing this edge.
    int cost;

    Edge(size_t start, size_t end, int cost);
};

bool operator==(Edge left, Edge right);
bool operator!=(Edge left, Edge right);

/// A node in a graph.
struct Node
{
    /// The index of this node in the containing digraph.
    size_t index;
    /// The name of this node.
    std::string name;
    /// The edges that connect this node to other nodes.
    std::vector<Edge> edges;

    Node(size_t index, std::string name);

    /// Returns the edge that connects this node to the node with the supplied index.
    Edge find_edge(size_t node) const;
};

/// A directed graph.
struct Digraph
{
    /// The nodes in this graph.
    std::vector<Node> nodes;

    Digraph(size_t size, const std::vector<int>& costs);
};

#define NA 0

/// The four-city directed graph.
static const Digraph SMALL_DIGRAPH{4, {
    NA, 1,  3,  8,
    5,  NA, 2,  6,
    1,  18, NA, 10,
    7,  4,  12, NA,
}};

/// The fifteen-city directed graph.
static const Digraph LARGE_DIGRAPH{15, {
    NA, 29, 82, 46, 68, 52, 72, 42, 51, 55, 29, 74, 23, 72, 46,
    29, NA, 55, 46, 42, 43, 43, 23, 23, 31, 41, 51, 11, 52, 21,
    82, 55, NA, 68, 46, 55, 23, 43, 41, 29, 79, 21, 64, 31, 51,
    46, 46, 68, NA, 82, 15, 72, 31, 62, 42, 21, 51, 51, 43, 64,
    68, 42, 46, 82, NA, 74, 23, 52, 21, 46, 82, 58, 46, 65, 23,
    52, 43, 55, 15, 74, NA, 61, 23, 55, 31, 33, 37, 51, 29, 59,
    72, 43, 23, 72, 23, 61, NA, 42, 23, 31, 77, 37, 51, 46, 33,
    42, 23, 43, 31, 52, 23, 42, NA, 33, 15, 37, 33, 33, 31, 37,
    51, 23, 41, 62, 21, 55, 23, 33, NA, 29, 62, 46, 29, 51, 11,
    55, 31, 29, 42, 46, 31, 31, 15, 29, NA, 51, 21, 41, 23, 37,
    29, 41, 79, 21, 82, 33, 77, 37, 62, 51, NA, 65, 42, 59, 61,
    74, 51, 21, 51, 58, 37, 37, 33, 46, 21, 65, NA, 61, 11, 55,
    23, 11, 64, 51, 46, 51, 51, 33, 29, 41, 42, 61, NA, 62, 23,
    72, 52, 31, 43, 65, 29, 46, 31, 51, 23, 59, 11, 62, NA, 59,
    46, 21, 51, 64, 23, 59, 33, 37, 11, 37, 61, 55, 23, 59, NA,
}};

#undef NA

}
