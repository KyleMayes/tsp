#!/usr/bin/env python

import pygal
import subprocess
import sys

def benchmark(executable, dataset):
    print '  {}'.format(dataset)
    output = subprocess.check_output([executable, 'benchmark', dataset])[:-1]
    print '    {}ns'.format(output)
    output = output.split(':')
    return (int(output[0]), float(output[1]))

small = []
large = []

for solution in sys.argv[1:]:
    print '{}:'.format(solution)
    executable = './{}'.format(solution)
    small.append((solution, benchmark(executable, 'small')))
    large.append((solution, benchmark(executable, 'large')))

def chart(dataset, name, cities, unit):
    chart = pygal.Bar(legend_at_bottom=True)
    chart.style = pygal.style.CleanStyle
    chart.title = '{}-city TSP (average of {} samples)'.format(cities, dataset[0][1][0])
    chart.y_title = unit[0]

    for benchmark in dataset:
        chart.add('{}'.format(benchmark[0]), benchmark[1][1] / unit[1])

    chart.render_to_file('benchmark-{}.svg'.format(name))

chart(small, 'small', 4, ('Nanoseconds', 1.0))
chart(large, 'large', 15, ('Seconds', 1000000000.0))
